## Getting Started

for now, there's (mostly) one entrypoint:

```sh
$ racket cli/run.rkt help
$ racket cli/run.rkt gui projects
$ racket cli/run.rkt gui project /path/to/my/project.stele1.d climbs
```

## About

This project was mostly motivitated by curiousity around
[ActivityLog2](https://github.com/alex-hhh/ActivityLog2)
(see [screenshots here](https://alex-hhh.github.io/2021/09/screenshots.html)),
[Native Applications with Racket](https://defn.io/2020/01/04/remember-internals/),
and a
[few](https://defn.io/2021/11/16/lisp-gui-examples/)
[other](https://blog.matthewdmiller.net/series/survey-of-the-state-of-gui-programming-in-lisp)
[articles](https://defn.io/2019/06/17/racket-gui-saves/).

## development

The first and only concrete goal is a GUI that supports all of stele1.
Then decent maps.
Later, optionally, will be CLI features.

<!--

Most simply, this should be possible

```sh
$ racket main.rkt edit /path/to-project.stele1.d::climb::"The Nose"
$ # or
$ racket main.rkt edit /path/to-project.stele1.d::climbs
$ racket main.rkt create /path/to-project.stele1.d::area::"Roadside"
$ racket main.rkt view-map /path/to-project.stele1.d
```

Eventually opening multiple UUIDs

```sh
$ racket main.rkt edit /path/to-project.stele1.d::climb:: [UUIDs]
$ # this should open multiple windows
```

Or more ambitious stuff

```sh
$ racket main.rkt dump /path/to-project.stele1.d::climb::"The Nose" \
$> | racket main.rkt create ./another/climbing/destination.stele1.d::climb::-
```

-->

## TODO

- handle HEIC images
- maps
  - [`map-widget`](https://docs.racket-lang.org/map-widget/index.html)
  - [ActivityLog2](https://github.com/alex-hhh/ActivityLog2)
