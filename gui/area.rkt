#lang racket/gui

(require racket/gui/base)

(provide area-frame%)

(define header-font
  (make-object font% 20 'default 'normal 'bold))

(define area-frame%
  (class object%
    (init area)
    (define p (send area get-project))
    (define a area)
    (define frame (new frame% [label (send p get-name)] [width 500] [height 600]))
    (define attrs (new vertical-panel% [parent frame]))
    (define name (new message% [parent attrs] [label (send a get-name)] [font header-font]))
    (define parent-proj
      (new message% [parent attrs] [label (string-append "FROM: " (send (send a get-project) get-name))]))
    (super-new)
    (define/public (show) (send frame show #t))
    (define/public (close) (send frame show #f))))
