#lang racket

(provide chunk)

(define (chunk c l)
  (define (popnum accum count rest)
    (cond ((empty? rest) (list (reverse accum) rest))
          ((= count 0) (list (reverse accum) rest))
          (else (popnum (cons (car rest) accum)
                        (sub1 count) 
                        (cdr rest)))))
  (let loop ([accum '()]
             [rest  l])
    (let* ([popped (popnum '() c rest)]
           [a (car popped)]
           [b (cadr popped)])
      (cond ((empty? b) (reverse (cons a accum)))
             (else      (loop (cons a accum) b))))))
